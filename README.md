# ZSH Config

Configuration of the [ZSH](https://en.wikipedia.org/wiki/Z_shell) shell.

The configuration can be automatically updated with the [update.sh](./update.sh) script, using the following Command:

    curl -s https://gitlab.com/frPursuit/zsh-config/-/raw/master/update.sh | sudo bash
