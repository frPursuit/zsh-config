#!/bin/bash

#######################################################
# Utility script used to update the ZSH configuration #
#             from the GitLab repository              #
#######################################################

set -e # Stop if any command fails
echo "Updating zsh configuration..."

BASE_URL="https://gitlab.com/frPursuit/zsh-config/-/raw/master"

echo "Updating directory colors"
curl -s "$BASE_URL/dir_colors" > /etc/dir_colors

mkdir -p /etc/zsh

for file in newuser.zshrc.recommended zlogin zlogout zprofile zshenv zshrc
do
    echo "Updating \"$file\""
    curl -s "$BASE_URL/zsh/$file" > /etc/zsh/$file
done
